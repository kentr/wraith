/**
 * Javascript driver to record node search results.
 * Expects:
 *   <form id="search-block-form"...>
 *   <input type="text" name="search_block_form" ...>
 */
module.exports = function (casper, ready) {

    casper.wait(2000, function() {
      // Submit form#search-block-form,
      // with value of "about" in the field
      // "search_block_form".
      this.fill('form#search-block-form', {
          'search_block_form':    'about',
      }, true);
      ready();
    });
}
