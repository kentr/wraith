# README #

This is a set of wrapper scripts and skeleton configs to get started quickly on using [Wraith](https://bbc-news.github.io/wraith/os-1. install.html) for smoke-testing on a given project.

The goal is to be able to setup and engage Wraith with no interaction for use as a smoke test with automated updates, automated deployments, etc.

In short, it automates these steps:

* Run `wraith capture` to compare predefined sites, using a common set of paths (test URLs).  
    * For prod-to-prod comparison, it uses wraith's `history` and `latest` modes.
* Compress those screenshots with `optipng`.
* Open the result in a browser for your inspection.
* Accumulate archives of the comparisons over time.
* Build an `index.html` file with links to the archived results (for maintaining records and comparison to past results).
* Upload the results to a server where others can see them.

## Requirements ##

* Ruby
* Wraith
* Optipng
* For comparing against remote sites (stage, prod), internet access is required.
* To automatically upload the screenshots to a server so that others can see them, SSH access to that server is required.

## Setup ##

1. Clone this repo into your preferred location.  I put it in a subfolder named `wraith` within my project, but it shouldn't matter where you put it.  It doesn't reference the web project code, so it can be on a separate server.
1. Install [Wraith](https://bbc-news.github.io/wraith/os-1. install.html) on the same machine.
1. Copy these config files to the new names specified:

    * `configs/example.smoke.*.yml` -> `configs/smoke.*.yml`
    * `scripts/example.config.sh` -> `scripts/config.sh`

1. Edit the configs as needed.  The comments within should explain the options.  Here's a summary of what the config files represent:

    * `smoke.common.yml` - Configs that are shared between the different environment configs, such as paths to test, general Wraith options, etc.
    * `smoke.<env>.yml` - Environment-specific configs, such as the URLs to the sites for testing.
    * `config.sh` - Variables used by `rsync` for uploading the results to a server.

## Running tests ##

If you don't want the image compression and uploading, you can still use standard `wraith` commands, such as this:

*  `wraith capture configs/smoke.local.yml`
    
If you the full automation, then use the various `run-*` scripts in `scripts`.  

For example, to do the run comparing `local` or `stage` against `prod`, you'd use one of these commands:

* `scripts/run-local.sh`
* `scripts/run-stage.sh`

Full runs take a while.  Much of that is due to the image compression, so you can omit that step if you want.  

For now you'll have to comment it out in the `<env>-vs-prod.sh` file for the environment in question.  Would be nice to have a command line option for skipping the image compression.

## Choosing website paths to test ##

* Pages that present differently on each page load don't test as well because the screen capture will inherently vary and give more false positives.
* Wraith seems to choke on URLs that contain non-ASCII characters.
* I'm working on some queries to automate path generation for  Drupal sites, and eventually for WordPress sites.