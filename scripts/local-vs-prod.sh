#! /usr/bin/env bash

cd $WRAITH_DIR
# echo '---> Working directory: '`pwd`

# Run wraith smoke test on local.
echo "---> Creating local-vs-prod comparison shots."
wraith capture configs/smoke.local.yml > /dev/null
ret=$?

open shots_local/gallery.html

# Compress shots.
echo "---> Compressing shots."
for i in `find shots_local -name \*.png`; do optipng --quiet $i; done

# Set up variables.
DATE=`date "+%Y%m%d-%H%M"`
DATE_HUMAN=`date "+%Y-%m-%d, %l:%M%p %Z"`
ARCHIVE_DIR=shots_local.$DATE

# Make archive copy.
echo "---> Creating archive directory: ${ARCHIVE_DIR}"
cp -Rp shots_local $ARCHIVE_DIR

# Add link to index-local.html file.
echo "---> Amending index-local.html"
echo "<a href='${ARCHIVE_DIR}/gallery.html'>local vs prod, ${DATE_HUMAN}</a> <br />" >> index-local.html

cd -

exit $ret
