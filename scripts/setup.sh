#!/usr/bin/env bash

# Directory of this script.
export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export WRAITH_DIR=${SCRIPT_DIR}/..

. $SCRIPT_DIR/config.sh
