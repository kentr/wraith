#! /usr/bin/env bash

echo "--> Running wraith smoke test on stage."

# Directory of this script.
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

. $SCRIPT_DIR/setup.sh

# Currently not necessary b/c "capture" mode automatically makes a new base.
# Make new prod base for comparison.
# echo '---> Creating new base shots of prod.'
# $SCRIPT_DIR/prod.base.sh

# Run stage-vs-prod comparison.
$SCRIPT_DIR/stage-vs-prod.sh

# push test results to stage.

echo '---> Copying results to stage'
rsync -au $WRAITH_DIR/ $WRAITH_RSYNC_DEST_HOST:$WRAITH_RSYNC_DEST_DIR  --include=shots --include=shots_stage* --include=shots_stage** --include=index.html --exclude=* --delete
ssh $WRAITH_RSYNC_DEST_HOST chmod -R 755 $WRAITH_RSYNC_DEST_DIR
