#! /usr/bin/env bash

echo "--> Running wraith smoke test on local."

# Directory of this script.
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

. $SCRIPT_DIR/setup.sh

# Currently not necessary b/c "capture" mode automatically makes a new base.
# Make new prod base for comparison.
# echo '---> Creating new base shots of prod.'
# $SCRIPT_DIR/prod.base.sh

# Run local-vs-prod comparison.
$SCRIPT_DIR/local-vs-prod.sh
