#! /usr/bin/env bash

cd $WRAITH_DIR
# echo '---> Working directory: '`pwd`

# Run wraith smoke test on dev.
echo "---> Creating dev-vs-prod comparison shots."
wraith capture configs/smoke.dev.yml > /dev/null
ret=$?

open shots_dev/gallery.html

# Compress shots.
echo "---> Compressing shots."
for i in `find shots_dev -name \*.png`; do optipng -quiet $i; done

# Set up variables.
DATE=`date "+%Y%m%d-%H%M"`
DATE_HUMAN=`date "+%Y-%m-%d, %l:%M%p %Z"`
ARCHIVE_DIR=shots_dev.$DATE

# Make archive copy.
echo "---> Creating archive directory: ${ARCHIVE_DIR}"
cp -Rp shots_dev $ARCHIVE_DIR

# Add link to index.html file.
echo "---> Amending index.html"
echo "<a href='${ARCHIVE_DIR}/gallery.html'>dev vs prod, ${DATE_HUMAN}</a> <br />" >> index.html

cd -

exit $ret
