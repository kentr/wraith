#! /usr/bin/env bash

echo '--> Creating "latest" shots for comparison to history.'

cd $WRAITH_DIR
# echo '---> Working directory: '`pwd`

# Run wraith smoke test on prod.
echo "---> Creating prod-vs-prod comparison shots."
wraith latest configs/smoke.prod.yml > /dev/null
ret=$?

open shots_prod/gallery.html

# Compress shots.
echo "---> Compressing shots."
for i in `find shots_prod -name \*.png`; do optipng --quiet $i; done

# Set up variables.
DATE=`date "+%Y%m%d-%H%M"`
DATE_HUMAN=`date "+%Y-%m-%d, %l:%M%p %Z"`
ARCHIVE_DIR=shots_prod.$DATE

# Make archive copy.
echo "---> Creating archive directory: ${ARCHIVE_DIR}"
cp -Rp shots_prod $ARCHIVE_DIR

# Add link to index.html file.
echo "---> Amending index.html"
echo "<a href='${ARCHIVE_DIR}/gallery.html'>Prod vs prod (before / after), ${DATE_HUMAN}</a> <br />" >> index.html

chmod -R 755 shots_prod* index.html

cd -

exit $ret
