#! /usr/bin/env bash

echo "--> Running wraith smoke test on dev."

# Directory of this script.
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

. $SCRIPT_DIR/setup.sh

# Currently not necessary b/c "capture" mode automatically makes a new base.
# Make new prod base for comparison.
# echo '---> Creating new base shots of prod.'
# $SCRIPT_DIR/prod.base.sh

# Run dev-vs-prod comparison.
$SCRIPT_DIR/dev-vs-prod.sh

# push test results to dev.

echo '---> Copying results to stage'
rsync -au $WRAITH_DIR/ $WRAITH_RSYNC_DEST_HOST:$WRAITH_RSYNC_DEST_DIR  --include=shots --include=shots_dev* --include=shots_dev** --include=index.html --exclude=* --delete
ssh $WRAITH_RSYNC_DEST_HOST chmod -R 755 $WRAITH_RSYNC_DEST_DIR
