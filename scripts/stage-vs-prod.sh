#! /usr/bin/env bash

cd $WRAITH_DIR
# echo '---> Working directory: '`pwd`

# Run wraith smoke test on stage.
echo "---> Creating stage-vs-prod comparison shots."
wraith capture configs/smoke.stage.yml > /dev/null
ret=$?

open shots_stage/gallery.html

# Compress shots.
echo "---> Compressing shots."
for i in `find shots_stage -name \*.png`; do optipng -quiet $i; done

# Set up variables.
DATE=`date "+%Y%m%d-%H%M"`
DATE_HUMAN=`date "+%Y-%m-%d, %l:%M%p %Z"`
ARCHIVE_DIR=shots_stage.$DATE

# Make archive copy.
echo "---> Creating archive directory: ${ARCHIVE_DIR}"
cp -Rp shots_stage $ARCHIVE_DIR

# Add link to index.html file.
echo "---> Amending index.html"
echo "<a href='${ARCHIVE_DIR}/gallery.html'>stage vs prod, ${DATE_HUMAN}</a> <br />" >> index.html

chmod -R 755 shots_stage* index.html

cd -

exit $ret
