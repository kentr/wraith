#!/usr/bin env

# SSH host to which results will be posted.
export WRAITH_RSYNC_DEST_HOST='ssh-host-name'
# Directory on SSH host to which results will be posted.
export WRAITH_RSYNC_DEST_DIR=path/to/vr-shots/
