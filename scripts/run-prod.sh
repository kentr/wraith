#! /usr/bin/env bash

echo "-> Running wraith smoke test on prod."

# Directory of this script.
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

. $SCRIPT_DIR/setup.sh

# Run prod-vs-prod comparison.
$SCRIPT_DIR/prod-vs-prod.sh

# Make new prod base on any updates for *next* comparison.
$SCRIPT_DIR/prod.base.sh

# push test results to stage.

echo '---> Copying results to stage'
rsync -au $WRAITH_DIR/ $WRAITH_RSYNC_DEST_HOST:$WRAITH_RSYNC_DEST_DIR  --include=shots --include=shots_prod* --include=shots_prod** --include=index.html --exclude=* --delete
ssh $WRAITH_RSYNC_DEST_HOST chmod -R 755 $WRAITH_RSYNC_DEST_DIR
