#! /usr/bin/env bash

echo '--> Creating new shots_prod_base for next comparison.'

# Directory of this script.
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

. $SCRIPT_DIR/setup.sh

cd $WRAITH_DIR

# Remove previous base.
echo '---> Removing former shots_prod_base'
rm -r shots_prod_base

# Make new base.
echo "---> Creating shots_prod_base."
wraith history configs/smoke.prod.yml > /dev/null

cd -
